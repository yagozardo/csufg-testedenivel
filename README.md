# CONSTRUÇÃO DE SOFTWARE - INF/UFG - 18/1
Repositorio para a disciplina de CONSTRUÇÃO DE SOFTWARE do curso de Sistemas de Informação da Universidade Federal de Goiás.
## Projeto 1 - Nivelamento da Turma
### Membros
* [Yago Zardo](https://gitlab.com/yagozardo)

### Tecnologia Usada
* [Linguagem Swift](https://swift.org)