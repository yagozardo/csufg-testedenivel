//
//  main.swift
//  Construcao
//
//  Created by Yago Zardo on 14/03/18.
//  Copyright © 2018 Yago Zardo. All rights reserved.
//

import Foundation

func getWeekdayFrom(_ stringDate: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyyMMdd"
    
    let dateFormatter2 = DateFormatter()
    dateFormatter2.dateFormat = "EEEE"
    
    guard let date = dateFormatter.date(from: stringDate) else {
        return "\nThe date entered does not conform to the format yyyyMMdd\n"
    }
    let weekday: String = dateFormatter2.string(from: date)
    return weekday
}


print("Please input the desired date in the format yyyyMMdd\n")

if let response = readLine() {
    if response.count > 0 {
        let date: String = getWeekdayFrom(response)
        print(date)
    } else {
        print("Nothing was entered!")
    }
} else {
    print("Nothing was entered!")
}


